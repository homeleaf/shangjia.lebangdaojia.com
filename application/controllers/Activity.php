<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*  优惠活动
*/

class Activity extends MY_Controller {
	public function __construct(){
		parent::__construct();

        $this->me_id = $this->session->userdata('me_id');
        $this->me_name = $this->session->userdata('me_username');
        $this->load->library('lb_redis');
	}

	//首页
            public function index(){

            $coupon_id = 52;

            $had_assign = Lb_redis::get('coupon_assign_'.$this->me_id.'_'.$coupon_id);

            $this->load->view('activity/index',[
             'merchant_id'=>$this->me_id,
                'coupon_id'=>$coupon_id,
            'had_assign'=>$had_assign ? '1' : '0'
            ]);
 }

    //领取优惠券
    public function coupon_assign($merchant_id,$coupon_id,$count){

        //通知后台
        $this->load->library('admin_server');
        $ret_arr = $this->admin_server->coupon_assign_call($merchant_id,$coupon_id,$count);
        //如果失败，记录日志
        if(empty($ret_arr) || $ret_arr['code']!=200){
            ajax_response(1, '系统繁忙，请重试');
        }else if (!Lb_redis::set('coupon_assign_'.$merchant_id.'_'.$coupon_id,$count)){
            ajax_response(1, '系统繁忙，请重试');
        }else{
            ajax_response(0, 'success');
        }
    }

}