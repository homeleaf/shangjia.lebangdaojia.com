<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*  师傅
*/

class Master extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}

	//发现
public function discover(){
        
        $data = [];
        $data['__provinces'] = get_province();  

        $district_id = $this->input->get('district'); 
        $city_id = $this->input->get('city');
        $province_id =$this->input->get('province');   
        $service_type = $this->input->get('service_type');
        $page = $this->input->get('p') ? : 1;
        //log_message('error','discover '.$district_id.' city_id '.$city_id.'province_id '.$province_id.' page '.$page.' service_type '.$service_type);

        $data['district_id'] = $district_id;
        $data['service_type'] = $service_type;
        
        $num_per_page = config_item('num_per_page');
        $this->load->model('master_model');
        $qiniu = config_item('qiniu');
        $count = $this->master_model->get_suitable_user_num($district_id,$service_type,$city_id,$province_id);
        //log_message('error','discover count '.$count);
        $data['count'] = $count;

        $users = $this->master_model->get_suitable_user($district_id,$service_type,$page,$num_per_page['order_index'],$city_id,$province_id);
        foreach ($users as $key => &$user) {
            $user['idcard_hold'] = $user['idcard_hold']? (strpos($user['idcard_hold'],'http') === 0 ? $user['idcard_hold']:$qiniu['source_url'].$user['idcard_hold']) : asset('images/default_head.png');
            $user['service_type_text'] = implode(',',getServiceTypeText(explode(',',$user['service_type'])));
            $user['service_area_text'] = implode(',',getServiceAreasText(explode(',',$user['service_area_ids'])));

                $user['avg_score'] = sprintf('%.0f',$user['order_count'] ? '5': 0);//$user['score_sum']/$user['score_count'] 
                $user['praise_rate'] = sprintf('%.2f',$user['order_count'] ? 100 : 0);
                $user['stars'] = getStars($user['points']);

                $user['extra_count'] = count(array_filter([
                    $user['extra_finish_in'],
                    $user['extra_nothing_fee'],
                    $user['extra_repair_free'],
                    $user['extra_floor_free'],
                    $user['extra_carry_fee'],
                    $user['extra_far_fee'],
                    $user['extra_tmall_examine'],
                    $user['extra_storage'],
                    $user['extra_move_free'],
                    ]));
            }
            $data['list'] = $users;

            $this->load->library('pag');
            $this->pag->config($count,$num_per_page['order_index'],3);
            $data['__pagination_url'] = $this->pag;
            $areas = getAreas();
            if ($district_id && strcmp($district_id, '请选择区')){
                //log_message('error','discover 1111 '.strcmp($district_id, '请选择区'));            
                $district = @$areas[$district_id];
                $city = @$areas[$district['p']];
                $province = @$areas[$city['p']];
                $data['provinceName'] = empty($province['n']) ? '请选择省':$province['n'];
                $data['provinceId'] = $province['i'];
                $data['cityName'] = $city['n'];
                $data['cityId'] = $city['i'];
                $data['districtName'] = $district['n'];
                $data['districtId']=$district['i'];
            } elseif($city_id && strcmp($city_id, '请选择市')) {
                $data['provinceName'] = $areas[$province_id]['n'];
                $data['provinceId'] = $province_id;
                $data['cityName'] = $areas[$city_id]['n'];
                $data['cityId'] = $city_id;

                $data['districtName'] =  '请选择区';       
                $data['districtId']='';
            } elseif($province_id && strcmp($province_id, '请选择省')) {
                 //log_message('error','discover 3333 ');
                $data['provinceName'] = $areas[$province_id]['n'];
                $data['provinceId'] = $province_id;

                $data['cityName'] = '请选择市';
                $data['districtName'] =  '请选择区';        
                $data['cityId'] = '';
                $data['districtId']='';
            }  else {

                $data['provinceName'] = '请选择省';
                $data['cityName'] = '请选择市';
                $data['districtName'] =  '请选择区';
                $data['provinceId'] = '';
                $data['cityId'] = '';
                $data['districtId']='';
            }

            $this->load->view('master/discover',$data);
        }

    }

    function getServiceTypeText(array $service_types)
    {
        $service_type_config = config_item('service_type');
        $service_type_text = [];
        foreach ($service_types as $service_type){
            $service_type_text[] = @$service_type_config[$service_type];
        }
        return array_filter($service_type_text);
    }

    function getServiceAreasText(array $service_area_ids)
    {
        $areas = getAreas();
        $service_areas = [];
        foreach ($service_area_ids as $service_area_id){
            $service_areas[] = @$areas[$service_area_id]['n'];
        }
        return array_filter($service_areas);
    }

    function getStars($points)
    {
        switch (true){
            case $points == 0:
            return 0;
            case $points <= 5:
            return str_repeat('<img src="'.asset('images/bj2.png').'">',1);
            case $points <= 20:
            return str_repeat('<img src="'.asset('images/bj2.png').'">',2);
            case $points <= 45:
            return str_repeat('<img src="'.asset('images/bj2.png').'">',3);
            case $points <= 75:
            return str_repeat('<img src="'.asset('images/bj2.png').'">',4);
            case $points <= 125:
            return str_repeat('<img src="'.asset('images/bj2.png').'">',5);
            case $points <= 250:
            return str_repeat('<img src="'.asset('images/bj4.png').'">',1);
            case $points <= 500:
            return str_repeat('<img src="'.asset('images/bj4.png').'">',2);
            case $points <= 1000:
            return str_repeat('<img src="'.asset('images/bj4.png').'">',3);
            case $points <= 2500:
            return str_repeat('<img src="'.asset('images/bj4.png').'">',4);
            case $points <= 5000:
            return str_repeat('<img src="'.asset('images/bj4.png').'">',5);
            case $points <= 10000:
            return str_repeat('<img src="'.asset('images/bj5.png').'">',1);
            case $points <= 25000:
            return str_repeat('<img src="'.asset('images/bj5.png').'">',2);
            case $points <= 50000:
            return str_repeat('<img src="'.asset('images/bj5.png').'">',3);
            case $points <= 100000:
            return str_repeat('<img src="'.asset('images/bj5.png').'">',4);
            case $points > 100000:
            return str_repeat('<img src="'.asset('images/bj5.png').'">',5);
            default:
            return 0;
        }
        return 0;
    }