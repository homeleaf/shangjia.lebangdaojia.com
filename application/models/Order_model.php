<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 模型示例
 *
 * 这里继承了扩展后的核心模型基类
 */
class Order_model extends MY_Model {
	public function __construct(){
		$this->load->database();
	}

	public function get_order_by_oid($me_id, $order_id){
		$sql = "SELECT a.order_number, a.service_type, a.merchant_price, b.real_name, b.phone FROM orders a LEFT JOIN master b ON a.master_id=b.id WHERE a.id=$order_id AND a.merchant_id=$me_id LIMIT 1";
		$result = $this->db->query($sql)->row_array();
		log_message('error'," get_order_by_oid ".$sql);
		log_message('error'," get_order_by_oid ".$result['real_name'].$result['phone']);
		return $result;
	}

	public function get_refund_order($me_id, $order_id){
		$where = array(
			'merchant_id' => $me_id,
			'id' => $order_id
			);
		$result = $this->db->select('order_number,merchant_price,pay_type')->where($where)->get('orders')->row_array();

		$paytype_conf = config_item('pay_type');
		$result['pay_type'] = $paytype_conf[$result['pay_type']];
		return $result;
	}

	public function get_complain_order($me_id, $order_id){
		$sql = "SELECT a.order_number, a.merchant_price, a.master_name, b.phone FROM orders a LEFT JOIN master b ON a.master_id=b.id WHERE a.id=$order_id AND a.merchant_id=$me_id";
		$result = $this->db->query($sql)->row_array();
		return $result;
	}

	public function get_evaluate_order($me_id, $order_id){
		$sql = "SELECT a.order_number, a.service_type, a.master_name, b.phone FROM orders a LEFT JOIN master b ON a.master_id=b.id WHERE a.id=$order_id AND a.merchant_id=$me_id";
		$result = $this->db->query($sql)->row_array();

		$service_type = config_item('service_type');
		$result['service_type'] = isset($service_type[$result['service_type']]) ? $service_type[$result['service_type']] : '';
		return $result;
	}

	//检测用户与订单是否匹配
	public function check_power($me_id, $order_id){
		$where = array(
			'id' => $order_id,
			'merchant_id' => $me_id
			);
		$num = $this->db->from('orders')->where($where)->count_all_results();
		return ($num>0) ? true : false;
	}

	//获取不同种类订单的各种状态的数目（后台首页）
	public function get_distinct_order_num($me_id, $priced_type=0){
		if(!empty($priced_type)){
			$sql = "select s.merchant_status,s.refund_status,s.arbitrate_status,s.evaluate_status,s.complain_status,s.except_status from `orders` o LEFT JOIN orders_status s on o.id=s.order_id LEFT JOIN orders_detail c ON o.id=c.order_id where o.merchant_id=$me_id and o.order_type=$priced_type";
		}else{
			$sql = "select s.merchant_status,s.refund_status,s.arbitrate_status,s.evaluate_status,s.complain_status, s.except_status from `orders` o LEFT JOIN orders_status s on o.id=s.order_id LEFT JOIN orders_detail c ON o.id=c.order_id where o.merchant_id=$me_id";
		}

		$result = $this->db->query($sql)->result_array();
		$data = [
		'wait_priced'=>0,
		'wait_hired'=>0,
		'wait_pay'=>0,
		'under_service'=>0,
		'wait_accept'=>0,
		'wait_evaluate'=>0,
		'under_refund'=>0,
		'under_arbitrate'=>0,
		'under_complain'=>0,
		];
		foreach ($result as $row){  
		   if($row['except_status'] == 0) {
		   	if (($row['complain_status'] == 1)  ){//投诉处理中
           				$data['under_complain']++;
           				continue;
           			}
		   	switch ($row['merchant_status']) {
		   		case 1: //待报价
		   		$data['wait_priced']++;
		   		break;
		   		case 2://待雇佣
		   		$data['wait_hired']++;
		   		break;
		   		case 3://待托管费用
		   		$data['wait_pay']++;
		   		break;
		   		case 4://师傅服务中
		   		$data['under_service']++;
		   		break;
		   		case 5://师傅服务中
		   		$data['under_service']++;
		   		break;
		   		case 6://待确认验收
		   		$data['wait_accept']++;
		   		break;
		   		case 7://待评价
		   		if($row['evaluate_status'] == 0) {
		   			$data['wait_evaluate']++;
		   		}
		   		break;

		   		default:
		   		break;
		   	}
		}
        	}
        		return $data;
    	}

	//获取用户的订单总数
	public function get_order_num($me_id, $priced_type){
		$where = array(
			'merchant_id' => $me_id,
			'order_type' => $priced_type
			);
		return $this->db->from('orders')->where($where)->count_all_results();
	}

	//查找服务完成之后，15天之内都没有回复的所有订单
	public function get_orders_without_evaluate($expire){
		$deadline = time()-$expire;
		$sql = "SELECT a.id, a.merchant_id FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id WHERE b.finish_time<$deadline AND b.evaluate_status=0";
		$result = $this->db->query($sql)->result_array();
		return $result;
	}

	//用于报价订单的首页搜索记录数目
	public function get_baojia_search_num($me_id, $post){
		@extract($post); //'kehu','order_sn','logistics_no','ptype'
		$ptype = intval($ptype);
		$kehu = trim($kehu);
		$order_sn = trim($order_sn);
		$logistics_no = trim($logistics_no);

		$where = " WHERE a.merchant_id=$me_id ";
		$sql = '';
		switch ($ptype) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 6:
			case 7:
			$where .= " AND b.merchant_status=$ptype "." AND b.except_status =0 ";
			break;
			case 5:
			$where .= " AND (b.merchant_status=5 OR b.merchant_status=4) "." AND b.except_status =0 ";
			break;
			case 8:
			$where .= " AND b.refund_status=1 AND b.arbitrate_status=0 "." AND b.except_status !=1 "; 
			break;
			case 9:
			$where .= " AND b.arbitrate_status=1 "." AND b.except_status !=1 "; 
			break;
			case 10:
			$where .= " AND b.except_status=1 "; 
			break;
			case 11:
			$where .= " AND b.merchant_status=7 AND b.evaluate_status=0 "." AND b.except_status !=1 ";
			break;
			case 12:
			$where .= " AND b.complain_status=1 "." AND b.except_status !=1 ";
			break;
			default:
				# code...
			break;
		}
		if(!empty($order_sn)){
			$where .= " AND a.order_number like '%{$order_sn}%' ";
		}
		if(!empty($kehu)){
			$where .= " AND (c.customer_name like '%{$kehu}%' OR c.customer_phone like '%{$kehu}%') ";
		}
		if(!empty($logistics_no)){
			$where .= " AND c.logistics_ticketnumber like '%{$logistics_no}%'";
		}

		/*if($ptype != 10){
			$where .= " AND b.except_status!=1 "; 
		}*/
		if(empty($kehu) && empty($logistics_no)){
			$sql = "SELECT COUNT(*) as num FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id {$where}";
		}else{
			$sql = "SELECT COUNT(*) as num FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id LEFT JOIN orders_detail c ON a.id=c.order_id {$where}";
		}

		$result = $this->db->query($sql)->row_array();
		return $result['num'];
	}

	//用于报价订单的首页搜索记录详情
	public function get_baojia_search_item($me_id, $post, $page, $num_per_page){
		@extract($post); //'kehu','order_sn','logistics_no','ptype'
		$ptype = intval($ptype);
		$kehu = trim($kehu);
		$order_sn = trim($order_sn);
		$logistics_no = trim($logistics_no);
		$start = ($page-1)*$num_per_page;

		$where = " WHERE a.merchant_id=$me_id ";
		$order_by="a.id DESC";
		$sql = '';
		switch ($ptype) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 6:
			case 7:
			$where .= " AND b.merchant_status=$ptype "." AND b.except_status =0 ";
			break;
			case 5:
			$where .= " AND (b.merchant_status=5 OR b.merchant_status=4) "." AND b.except_status =0 ";
			break;
			case 8:
			$where .= " AND b.refund_status=1 AND b.arbitrate_status=0 "." AND b.except_status !=1 "; 
			break;
			case 9:
			$where .= " AND b.arbitrate_status=1 "." AND b.except_status !=1 "; 
			break;
			case 10:
			$where .= " AND b.except_status=1 "; 
			break;
			case 11:
			$where .= " AND b.merchant_status=7 AND b.evaluate_status=0 "." AND b.except_status !=1 ";
			break;
			case 12:
			$where .= " AND b.complain_status=1 "." AND b.except_status !=1 ";
			break;
			default:
				# code...
			break;
		}

		if($ptype ==6) {
            $order_by = "b.finish_time DESC";
        }

		if(!empty($order_sn)){
			$where .= " AND a.order_number like '%{$order_sn}%' ";
		}
		if(!empty($kehu)){
			$where .= " AND (c.customer_name like '%{$kehu}%' OR c.customer_phone like '%{$kehu}%') ";
		}
		if(!empty($logistics_no)){
			$where .= " AND c.logistics_ticketnumber like '%{$logistics_no}%'";
		}

		/*if($ptype != 10){
			$where .= " AND b.except_status!=1 "; 
		}*/

		$sql = "SELECT a.id, a.order_number, a.service_type, a.add_time, a.merchant_price, a.confirm_code, b.finish_time, b.merchant_status, b.except_status, b.refund_status, b.arbitrate_status, b.evaluate_status, c.customer_area, c.customer_address, c.customer_name, c.customer_phone, c.merchant_remark,c.customer_area_id,a.master_id,d.real_name,d.phone FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id LEFT JOIN orders_detail c ON a.id=c.order_id  LEFT JOIN master d ON a.master_id=d.id {$where} ORDER BY {$order_by} LIMIT $start, $num_per_page";
		log_message('error'," get_baojia_search_item ".$sql);
		$result = $this->db->query($sql)->result_array();

		$service_type = config_item('service_type');
		foreach ($result as $key => $val){
			$result[$key]['add_time'] = $ptype == 6 ? date('Y-m-d H:i', $val['finish_time']):date('Y-m-d H:i', $val['add_time']);
			$result[$key]['service_type'] = isset($service_type[$val['service_type']]) ? $service_type[$val['service_type']] : '';
			$result[$key]['master_num'] = $this->get_master_num($val['id']);
		}
		return $result;
	}

	public function get_master_num($order_id){
		$where = array('order_id' => $order_id);
		return $this->db->from('orders_offer')->where($where)->count_all_results();
	}

	//获取报价订单详情
	public function get_baojia_detail($me_id, $order_id){
		$result = array();
		$sql = "SELECT a.id, a.order_number, a.service_type, a.merchant_price, a.pay_time,a.pay_type,a.add_time as xiadan_time, b.merchant_status, b.except_status, b.refund_status, b.arbitrate_status, b.evaluate_status, b.logistics_status, b.finish_time, c.* FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id LEFT JOIN orders_detail c ON a.id=c.order_id WHERE a.id=$order_id AND a.merchant_id=$me_id";

		$result_order_info = $this->db->query($sql)->row_array();

		//整理数据
		$paytype_conf = config_item('pay_type');
		$result_order_info['pay_type'] = @$paytype_conf[$result_order_info['pay_type']];
		$result_order_info['pay_time'] = date('Y-m-d H:i', $result_order_info['pay_time']);
		$merchant_status = config_item('baojia_merchant_order_status');
		$result_order_info['merchant_status_txt'] = isset($merchant_status[$result_order_info['merchant_status']]) ? $merchant_status[$result_order_info['merchant_status']] : '';
		$result_order_info['xidan_time_txt'] = date('Y-m-d H:i', $result_order_info['xiadan_time']);
		$service_type = config_item('service_type');
		$local_service_type = $result_order_info['service_type'];
		$result_order_info['service_type_txt'] = isset($service_type[$result_order_info['service_type']]) ? $service_type[$result_order_info['service_type']] : '';
		$result_order_info['customer_elevator'] = ($result_order_info['customer_elevator']==1) ? '电梯' : '步梯';
		$result_order_info['customer_tmall_number'] = empty($result_order_info['customer_tmall_number']) ? "否" : "需要天猫师傅核销  ".$result_order_info['customer_tmall_number'];
		$result_order_info['logistics_status_txt'] = ($result_order_info['logistics_status']==2) ? '已到货' : '未到货';
		$result_order_info['logistics_ticketnumber'] = empty($result_order_info['logistics_ticketnumber']) ? '- -' : $result_order_info['logistics_ticketnumber'];
		$result_order_info['logistics_packages'] = empty($result_order_info['logistics_packages']) ? '- -' : $result_order_info['logistics_packages'];
		$result_order_info['logistics_name'] = empty($result_order_info['logistics_name']) ? '- -' : $result_order_info['logistics_name'];
		$result_order_info['logistics_phone'] = empty($result_order_info['logistics_phone']) ? '- -' : $result_order_info['logistics_phone'];
		$result_order_info['logistics_address'] = empty($result_order_info['logistics_address']) ? '- -' : $result_order_info['logistics_address'];
		$result_order_info['logistics_mark'] = empty($result_order_info['logistics_mark']) ? '- -' : $result_order_info['logistics_mark'];
		$result_order_info['merchant_finish_time'] = empty($result_order_info['merchant_finish_time']) ? '' : '希望师傅在'.date('Y-m-d', $result_order_info['merchant_finish_time']).'完成任务。';
		if(empty($result_order_info['customer_memark'])){
			if(empty($result_order_info['merchant_finish_time'])){
				$result_order_info['customer_memark'] = '- -';
			}else{
				$result_order_info['customer_memark'] = $result_order_info['merchant_finish_time'];
			}
		}else{
			$result_order_info['customer_memark'] .= '。'.$result_order_info['merchant_finish_time'];
		}

		$result_goods = array();
		$result_master = array();
		//此处判断，防止越权
		if(!empty($result_order_info)){
			$sql = "SELECT goods_type, goods_name, goods_mark, goods_num, goods_img FROM orders_goods WHERE order_id=$order_id";
			$result_goods = $this->db->query($sql)->result_array();
			$goods_type = config_item('goods');
			$qiniu = config_item('qiniu');
			foreach ($result_goods as $key => $val) {
				$result_goods[$key]['goods_type'] = isset($goods_type[$val['goods_type']]) ? $goods_type[$val['goods_type']] : '其他';
				$result_goods[$key]['goods_mark'] = empty($val['goods_mark']) ? '- -' : $val['goods_mark'];
				//当服务类型是维修时，goods_img字段是用逗号隔开的几个图片
				if($local_service_type == 3){
					$img_arr = explode(',', $val['goods_img']);
					foreach ($img_arr as $k => $v) {
						$img_arr[$k] = $qiniu['source_url'].$v;
					}
					$result_goods[$key]['goods_img'] = $img_arr;
				}else{
					$result_goods[$key]['goods_img'] = $qiniu['source_url'].$val['goods_img'];
				}
			}

			//获取师傅信息
			if($result_order_info['merchant_status'] > 2){
				$sql = "SELECT a.master_name, b.phone FROM orders a LEFT JOIN master b ON a.master_id=b.id WHERE a.id=$order_id AND a.merchant_id=$me_id";
				$result_master = $this->db->query($sql)->row_array();
			}
		}
		$result['order'] = $result_order_info;
		$result['goods'] = $result_goods;
		$result['master'] = $result_master;
		$result['master_num'] = $this->get_master_num($order_id);
		return $result;
	}

	//取消报价订单
	public function del_baojia_order($me_id, $order_id, $from){
		$final_result = false;
		$time = time();
		if($this->check_power($me_id, $order_id)){
			$sql = "UPDATE orders_status SET except_status=1, upd_time=$time WHERE order_id=$order_id";

			$this->db->trans_begin();
			$this->db->query($sql);

			if ($this->db->trans_status() === FALSE){
	    			$this->db->trans_rollback();
			}else{
	    			$this->db->trans_commit();
	    			$final_result = true;
			}
			log_message('error',' del_baojia_order '.$me_id.' order_id '.$order_id.'sql '.$sql);
		}
		return $final_result;
	}

	//获取服务节点
	public function get_baojia_trace($order_id){
		$sql ="SELECT merchant_status, master_status, except_status, refund_status, arbitrate_status, evaluate_status, door_time, deliver_imgs, deliver_except, finish_imgs, finish_ticket_img, tmall_check_img, finish_message, finish_time, appoint_time, deliver_time FROM orders_status WHERE order_id=$order_id";
		$result = $this->db->query($sql)->row_array();

		$week_arr = array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
		$week = empty($result['door_time']) ? 0 : date('w', $result['door_time']);
		$door_time = empty($result['door_time']) ? 0 : date('m月d日,H:i', $result['door_time']);
		$door_time_arr = explode(',', $door_time);
		$result['door_time'] = $door_time_arr[0].'（'.$week_arr[$week].'）'.$door_time_arr[1];

		$result['finish_time_txt'] = empty($result['finish_time']) ? 0 : date('Y-m-d H:i', $result['finish_time']);
		$result['appoint_time'] = empty($result['appoint_time']) ? 0 : date('Y-m-d H:i', $result['appoint_time']);
		$result['deliver_time'] = empty($result['deliver_time']) ? 0 : date('Y-m-d H:i', $result['deliver_time']);
		$result['deliver_imgs'] = empty($result['deliver_imgs']) ? array() : json_decode($result['deliver_imgs'], true);
		$result['finish_imgs'] = empty($result['finish_imgs']) ? array() : json_decode($result['finish_imgs'], true);
		$qiniu = config_item('qiniu');
		$result['finish_ticket_img'] = $qiniu['source_url'].$result['finish_ticket_img'];
		$result['tmall_check_img'] = empty($result['tmall_check_img']) ? '' : $qiniu['source_url'].$result['tmall_check_img'];
		foreach ($result['deliver_imgs'] as $key => $val) {
			$result['deliver_imgs'][$key] = $qiniu['source_url'].$val;
		}
		foreach ($result['finish_imgs'] as $key => $val) {
			$result['finish_imgs'][$key] = $qiniu['source_url'].$val;
		}
		
		return $result;
	}

	//获取订单状态
	public function get_order_status($order_id){
		$sql ="SELECT merchant_status, master_status, except_status, refund_status, arbitrate_status, evaluate_status, finish_time FROM orders_status WHERE order_id=$order_id";
		$result = $this->db->query($sql)->row_array();
		return $result;
	}

	//查看师傅报价
	public function get_master_offer($order_id){
		$sql = "SELECT a.price, a.status, a.message,a.add_time,b.phone, b.real_name, b.idcard_hold, b.id FROM orders_offer a LEFT JOIN master b ON a.master_id=b.id WHERE a.order_id=$order_id";
		$base = $this->db->query($sql)->result_array();
		//基本信息，key为master_id
		$base = array_column($base, null, 'id');
		$qiniu = config_item('qiniu');
		foreach ($base as $key => $val) {
			//判断，如果是全路径，不加前缀
			if(empty($val['idcard_hold'])){
				$base[$key]['idcard_hold'] = asset("images/default_head.png");
			}else if(stripos($val['idcard_hold'], 'http') === FALSE){
				$base[$key]['idcard_hold'] = $qiniu['source_url'].$val['idcard_hold'];
			}
			//商家展示价格是师傅的1.1倍
			$base[$key]['price'] = $val['price']*1.1;
			$base[$key]['off_time'] = date('Y-m-d H:i', $val['add_time']);
		}

		$master_str = implode(',',array_keys($base));

		//统计信息
		$sql = "SELECT * FROM master_statistic WHERE master_id in ({$master_str})";
		$statistic = $this->db->query($sql)->result_array();
		$statistic = array_column($statistic, null, 'master_id');
		foreach ($statistic as $key => $val) {
			if(!empty($val['evaluate_count'])){
				$statistic[$key]['good_rat'] = sprintf('%.2f',$val['evaluate_count'] ? $val['evaluate_praise_count']/$val['evaluate_count']*100 : 0).'%';
			}else{
				if($statistic[$key]['order_count'] == 0) {
				     $statistic[$key]['good_rat'] ='      0   ';
				} else {
				    $statistic[$key]['good_rat'] = '100%';
				}
				
			}
			if(!empty($val['score_count'])){
				$statistic[$key]['score_sum'] = sprintf('%.2f',$val['score_count'] ? $val['score_sum']/$val['score_count'] : 0);
			}else{
				if($statistic[$key]['order_count'] == 0) {
				     $statistic[$key]['score_sum'] = '0';	
				} else {
				    $statistic[$key]['score_sum'] = '5';	
				}
				
			}
			$statistic[$key]['__score_icon'] = create_master_level_icon($val['points']);
		}

		//保证金
		$sql = "SELECT master_id, assure_fund FROM master_wallet WHERE master_id in ({$master_str})";
		$fund = $this->db->query($sql)->result_array();
		$fund = array_column($fund, null, 'master_id');

		$result = array(
			'base' => $base,
			'statistic' => $statistic,
			'fund' => $fund
			);

		return $result;
	}

	//解除师傅雇佣
	public function unhire_master($order_id, $hired_id){
		$where = array(
			'order_id' => $order_id,
			'master_id' => $hired_id
			);
		$data = array(
			'status' => 0
			);
		log_message('error',' unhire_master hired_id '.$hired_id.' order_id '.$order_id);
		$this->db->update('orders_offer', $data, $where);
	}

	//雇佣师傅
	public function hire_master($me_id, $order_id, $master_id, $hired_id){
		//查询订单入选报价金额，放在最开始，避免伪造
		$ret = $this->db->query("SELECT a.price, b.real_name FROM orders_offer a LEFT JOIN master b ON a.master_id=b.id WHERE a.order_id=$order_id AND a.master_id=$master_id")->row_array();
		if(empty($ret)){
			return false;
		}
		$master_price = $ret['price'];
		$master_name = $ret['real_name'];
		$merchant_price = $master_price*1.1;
		$time = time();
		$final_result  = false;

		$this->db->trans_begin();
		$sql = "UPDATE orders_offer SET status=1, upd_time=$time WHERE order_id=$order_id AND master_id=$master_id";
		$sql_order = "UPDATE orders_status SET merchant_status=3, upd_time=$time WHERE order_id=$order_id";

		//如果是首次雇佣，而不是替换师傅，需要更改统计
		if(empty($hired_id)){
			//更改订单状态
			$this->db->query($sql_order);
		}
		$this->db->query($sql);
		//更新orders表的价格
		$this->db->query("UPDATE orders SET master_id=$master_id, merchant_price=$merchant_price, offer_price=$master_price, master_name='{$master_name}', upd_time=$time WHERE id=$order_id");
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
		}else{
    		$this->db->trans_commit();
    		$final_result = true;
		}
		log_message('error',' hire_master '.$me_id.' order_id '.$order_id.'sql '.$sql.' sql order '.$sql_order);
		return $final_result;
	}

	//创建订单支付页面 订单信息
	public function get_order_for_pay($order_id){
		$sql = "SELECT a.id, a.merchant_id, a.order_number, a.service_type, a.merchant_price, a.master_id, b.customer_name, b.customer_phone, b.customer_address FROM orders a LEFT JOIN orders_detail b ON a.id=b.order_id WHERE a.id=$order_id";
		$result = $this->db->query($sql)->row_array();

		$service_type = config_item('service_type');
		$result['service_type'] = isset($service_type[$result['service_type']]) ? $service_type[$result['service_type']] : '- -';
		return $result;
	}

	//创建订单支付页面 师傅信息
	public function get_master_for_pay($master_id){
		$sql = "SELECT a.real_name, a.phone, b.assure_fund FROM master a LEFT JOIN master_wallet b ON a.id=b.master_id WHERE a.id=$master_id";
		return $this->db->query($sql)->row_array();
	}

	//查询总价
	public function get_total_fee($me_id, $order_id){
		$sql = "SELECT merchant_price, order_number, service_type, master_name, add_time FROM orders WHERE id=$order_id AND merchant_id=$me_id";
		$ret = $this->db->query($sql)->row_array();
		return $ret;
	}

	//产生订单
	public function create_order($me_id, $type, $order_no, $confirm_code, $post){
		@extract($post);
		$time = time();
		$ip = $this->input->ip_address();
		$final_result = false;

		$this->db->trans_begin();
		//如果非维修订单，将上传的货品加入仓库
		if(in_array($type, array(1,2,4))){
			$insert_value = '';
			//没有goods_id的，即为上传
			foreach($goods_id as $key => $val){
				if($val == 0){
					$insert_value .= "(null,'{$goods_name[$key]}','{$goods_img[$key]}','0.00',{$me_id},0,{$goods_type[$key]},{$time},'{$ip}','',1,0),";
				}
			}
			if(!empty($insert_value)){
				$insert_value = rtrim($insert_value, ',');
				$this->db->query("INSERT INTO goods VALUES {$insert_value}");
			}
		}
		//生成orders记录
		$this->db->query("INSERT INTO orders SET order_number='{$order_no}',merchant_id={$me_id},service_category=1,service_type={$type},add_time={$time},order_type=1,push_time={$time},confirm_code={$confirm_code}");
		$order_id = $this->db->insert_id();

		//货品加入orders_goods表
		if(in_array($type, array(1,2,4))){
			$insert_value = '';
			foreach($goods_id as $key => $val){
				$insert_value .= "(null,{$order_id},{$val},{$goods_type[$key]},'{$goods_name[$key]}','{$goods_remark[$key]}',{$goods_num[$key]},'{$goods_img[$key]}',0.00,{$time},0),";
			}
			$insert_value = rtrim($insert_value, ',');
		}else if($type==3){
			//目前维修只有一个货品，货品图片为逗号隔开的url
			$insert_value = '';
			foreach($goods_type as $key => $val){
				$insert_value .= "(null,{$order_id},0,{$goods_type[$key]},'','{$goods_remark[$key]}',0,'{$goods_img[$key]}',0.00,{$time},0),";
			}
			$insert_value = rtrim($insert_value, ',');
		}
		$this->db->query("INSERT INTO orders_goods VALUES {$insert_value}");
		//组合省市区文案
		$global_area = json_decode(get_province(), true);
		$district_id = $district;
		$areas = getAreas();
		$district = @$areas[$district];
		$city = @$areas[$district['p']];
		$province = @$areas[$city['p']];
		$area_txt = @implode('-',[$province['n'],$city['n'],$district['n']]);
		//更新orders_detail
		$this->db->query("INSERT INTO orders_detail SET order_id={$order_id},customer_name='{$customer_name}',customer_phone='{$customer_phone}',customer_area_id={$district_id},customer_address='{$address}',customer_elevator={$elevater},customer_floor={$floor},customer_tmall_number='{$tmall_number}',customer_memark='{$customer_remark}',logistics_packages={$goodnum},logistics_ticketnumber='{$logistics_no}',logistics_name='{$logistics_name}',logistics_phone='{$logistics_phone}',logistics_address='{$logistics_address}',logistics_consignee='{$logistics_consignee}',logistics_mark='{$logistics_remark}',merchant_name='{$me_name}',merchant_phone='{$me_phone}',merchant_finish_time='{$hope_finish_time}',add_time={$time},merchant_requirements='{$requirements}',customer_area='{$area_txt}'");
		//更新orders_status
		$this->db->query("INSERT INTO orders_status SET order_id={$order_id},merchant_status=1,logistics_status={$cargo_arrive},add_time={$time}");
		

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
			$final_result = true;
		}
		log_message('error',' creater_oder '.$me_id.' order_number '.$order_id.' merchant st =1');
		return $final_result ? $order_id : false;
	}

	public function check_confirmed($me_id, $order_id){
		$time = time();
		$final_result = false;
		$this->db->trans_begin();
		$this->db->query("UPDATE orders_status SET merchant_status=7, upd_time=$time WHERE order_id=$order_id");
		log_message('error',' check_confirmed '.$me_id.' order_id '.$order_id);
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
		}else{
    		$this->db->trans_commit();
    		$final_result = true;
		}
		return $final_result;
	}

	public function get_master_status($order_id){
		$where = array('order_id' => $order_id);
		$this->db->select('master_status')->from('orders_status')->where($where);
		$result = $this->db->get()->row_array();
		return $result['master_status'];
	}

	public function edit_baojia_order($order_id, $post){
		@extract($post);
		$time = time();
		$final_result = false;
		$this->db->trans_begin();
		$this->db->query("UPDATE orders_detail SET customer_name='{$customer_name}',customer_phone='{$customer_phone}',customer_address='{$address}',logistics_packages={$goodnum},logistics_ticketnumber='{$logistics_no}',logistics_name='{$logistics_name}',logistics_phone='{$logistics_phone}',logistics_address='{$logistics_address}',logistics_mark='{$logistics_remark}',merchant_name='{$me_name}',merchant_phone='{$me_phone}',upd_time={$time} WHERE order_id={$order_id}");
		$this->db->query("UPDATE orders_status SET logistics_status={$cargo_arrive},upd_time={$time} WHERE order_id={$order_id}");
		log_message('error',' edit_baojia_order  order_id '.$order_id);

		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
		}else{
    		$this->db->trans_commit();
    		$final_result = true;
		}
		return $final_result;
	}

	public function add_baojia_mark($order_id, $mark){
		$where = array(
			'order_id' => $order_id
			);
		$data = array(
			'merchant_remark' => $mark,
			'upd_time' => time()
			);
		$this->db->update('orders_detail', $data, $where);
		return $this->db->affected_rows();
	}

    	//用于报价订单的首页搜索记录数目
	public function get_search_num($me_id,$key){

		if($key){
			$where = " WHERE  merchant_id=$me_id and c.customer_name like '%$key%' OR c.customer_phone like '%$key%'";
		}else{
			$where = " WHERE 1=1 and merchant_id=$me_id";
		}
		$sql = "SELECT COUNT(*) as num FROM orders a LEFT JOIN orders_detail c ON a.id=c.order_id {$where}";
		log_message('error',' get_search_num '.$me_id.' '.$key.' '.$sql);
		$result = $this->db->query($sql)->row_array();
		return $result['num'];
	}

    	//用于报价订单的首页搜索记录详情
	public function get_search_item($me_id,$key,$page, $num_per_page){
		$start = ($page-1)*$num_per_page;

		if($key){
			$where = " WHERE merchant_id=$me_id and c.customer_name like '%$key%' OR c.customer_phone like '%$key%'";
		}else{
			$where = " WHERE 1=1";
		}

		$sql = "SELECT a.id, a.order_number, a.service_type, a.add_time, a.merchant_price, a.confirm_code, b.merchant_status, b.except_status, b.refund_status, b.arbitrate_status, b.evaluate_status, c.customer_area, c.customer_address, c.customer_name, c.customer_phone, c.merchant_remark,c.customer_area_id FROM orders a LEFT JOIN orders_status b ON a.id=b.order_id LEFT JOIN orders_detail c ON a.id=c.order_id {$where} ORDER BY a.id DESC LIMIT $start, $num_per_page";
		$result = $this->db->query($sql)->result_array();

		$service_type = config_item('service_type');
		foreach ($result as $key => $val){
			$result[$key]['add_time'] = date('Y-m-d H:i', $val['add_time']);
			$result[$key]['service_type'] = isset($service_type[$val['service_type']]) ? $service_type[$val['service_type']] : '';
			$result[$key]['master_num'] = $this->get_master_num($val['id']);
		}
		return $result;
	}

    	//最近的10条订单
	public function get_newest_item($number = 10){
		$sql = "select a.*, c.customer_area, c.customer_address, c.customer_name, c.customer_phone, c.merchant_remark,c.customer_area_id,m.me_username from (SELECT id, order_number, merchant_id,service_type, add_time FROM orders  ORDER BY id DESC LIMIT 2) a LEFT JOIN merchant m ON a.merchant_id=m.me_id LEFT JOIN orders_detail c ON a.id=c.order_id";
		$result = $this->db->query($sql)->result_array();

		$service_type = config_item('service_type');
		foreach ($result as $key => $val){
			$result[$key]['add_time'] = date('Y-m-d H:i', $val['add_time']);
			$result[$key]['service_type'] = isset($service_type[$val['service_type']]) ? $service_type[$val['service_type']] : '';
			$result[$key]['master_num'] = $this->get_master_num($val['id']);
			$result[$key]['customer_area'] = str_replace('-', '', $val['customer_area']);
			$result[$key]['before_time'] = timeTran($val['add_time']);
		}
		return $result;
	}

	public function add_replenish($me_id, $order_id, $order_number,$replenish_amount,$replenish_reason){
		$time = time();

		$data = [
		'order_number'=>$order_number,
		'order_id'=>$order_id,
		'replenish_amount'=>$replenish_amount,
		'replenish_reason'=>$replenish_reason,
		'add_time'=>$time,
		'upd_time'=>$time,
		];
		$this->db->insert('orders_replenish', $data);
		log_message('error',' add_replenish '.$me_id.' order_number '.$order_number.' order_id '.$order_id);
		return $this->db->insert_id();
	}

 
	public function get_back_phone($master_id) {
		$sql = "select backup_phone  from master_detail where master_id =$master_id ";	
		$result = $this->db->query($sql)->row_array();
		return $result ;
	}


	public function get_order_replenish_total($me_id, $order_id){
		$sql = "select sum(replenish_amount) as total from orders_replenish where order_id=$order_id and !ISNULL(pay_time)";
		$result = $this->db->query($sql)->row_array();
		return $result ? $result['total'] : 0;
	}

	public function get_order_replenish($me_id, $order_id){
		$sql = "select id,order_number,replenish_amount,replenish_reason,pay_time from orders_replenish where order_id=$order_id and !ISNULL(pay_time) order by pay_time asc";
		$result = $this->db->query($sql)->result_array();
		$paytype_conf = config_item('pay_type');
		foreach ($result as &$row){
			$sql = "select trade_number,source from merchant_trade_log where merchant_id=$me_id and status=1 and FIND_IN_SET({$row['id']},order_number_list)";
			$log = $this->db->query($sql)->row_array();
			if (!$log){
				continue;
			}
			$row['pay_type'] = $paytype_conf[$log['source']];

			$row['pay_time'] = date('Y-m-d H:i', $row['pay_time']);
		}
		return $result;
	}
}

function timeTran($time) {
	$t = time() - $time;
	$f = array(
		'31536000' => '年',
		'2592000' => '个月',
		'604800' => '星期',
		'86400' => '天',
		'3600' => '小时',
		'60' => '分钟',
		'1' => '秒'
		);
	foreach ($f as $k => $v) {
		if (0 != $c = floor($t / (int) $k)) {
			return $c . $v . '前';
		}
	}
}