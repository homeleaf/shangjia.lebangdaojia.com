<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php require 'application/views/basic/header.php'; ?>   
<?php require 'application/views/basic/top1.php'; ?>

<script>
    $(function(){
        var form = new Vue({
            el:'#div',
            data:{
                merchantId:'<?=$merchant_id?>',
                couponId:'<?=$coupon_id?>',
                submitText:'立即领取500元',
            },
            computed:{
            },
            mounted:function(){
            },
            watch: {
            },
            methods:{
                assign:function(event){
                    var self = this;

                    this.submitText = '领取中..';
                    if (!this.merchantId){
                        alert('请先登录');
                        location.href="<?=site_url('main/index/login')?>";
                        return;

                    }

                    if (!this.couponId){
                        alert('功能未上线，敬请期待');
                        this.submitText = '立即领取500元';
                        return;

                    }

                    axios.post('<?=site_url("activity/coupon_assign/$merchant_id/$coupon_id/50")?>')
                        .then(function (response) {
                            if (response.data.status == 0) {
                                alert('领取成功');
                                location.href = location.href;
                            } else {
                                alert(response.data.error || '领取失败');
                                self.submitText = '立即领取500元';
                                return;
                            }
                        },function (response) {
                            alert(response.data.error || '领取失败');
                            self.submitText = '立即领取500元';
                            return;
                        }).catch(function (err) {
                            alert('领取失败');
                            self.submitText = '立即领取500元';
                            return;
                        });

                }
            },
            components:{
            }
        });
    });
</script>

<div class="  huo2">
    <img src="<?=asset("images/huo1.png")?>" />

</div>
<div class="huodong" id="div">
    <div class="container">
        <div class="hou-1"><img src="<?=asset("images/huod1.png")?>" /></div>
        <center> <div class="huo-2">
                <div class=" huo-2-1"><img src="<?=asset("images/27.png")?>" /><br/>找师傅更便捷</div>
                <div class=" huo-2-1"><img src="<?=asset("images/28.png")?>" /><br/>诚信服务看得见</div>
                <div class=" huo-2-1"><img src="<?=asset("images/29.png")?>" /><br/>提供担保交易</div>
                <div class=" huo-2-1"><img src="<?=asset("images/30.png")?>" /><br/>货品损失先赔</div>
                <div class=" huo-2-1"><img src="<?=asset("images/31.png")?>" /><br/>服务已覆盖全国</div>

            </div></center><br/><br/>
        <div class="hou-11"><img src="<?=asset("images/huod2.png")?>" /></div>

        <div class="huo-3">
            <div class="huo-3-1">活动规则：</div>
            <div class="huo-3-2">1.新注册用户和旧用户都可以领取1次。</div>
            <div class="huo-3-2">2.领取的500元代金券金额以10元/张的形式发放，每个订单只能使用1张。</div>
            <div class="huo-3-2">3.优惠券使用有效期45天。</div>
            <div class="huo-3-2">4.活动时间为2017年7月27日-2017年8月31日;</div>


        </div>
        <div class="huo-4">
            <a v-if="<?=$had_assign?>" style="background: gray;">已领取</a>
            <a href="javascript:void(0)" v-else v-on:click="assign">{{submitText}}</a>
        </div>

    </div>


</div>

<?php require 'application/views/basic/bottom.php'; ?>

