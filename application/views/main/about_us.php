<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php require 'application/views/basic/header.php'; ?>   
<?php require 'application/views/basic/top1.php'; ?>


<div class="banner banner_gywm"></div>
<div class="gywm">
	<div class="container">
		<div class="gywm-1">
			<div class="top-2-2"
				style="text-align: center; border-bottom: 2px solid #ccc; padding-bottom: 15px;">
				<a <?=($about_type==0)?"class='on'":""?>
					href="<?=site_url('main/about_us')?>">公司简介</a> <a
					<?=($about_type==1)?"class='on'":""?>
					href="<?=site_url('main/about_us/1')?>">公司文化</a> <a
					<?=($about_type==2)?"class='on'":""?>
					href="<?=site_url('main/about_us/2')?>">发展历程</a> <a
					<?=($about_type==3)?"class='on'":""?>
					href="<?=site_url('main/about_us/3')?>">公司新闻</a> <a
					<?=($about_type==4)?"class='on'":""?>
					href="<?=site_url('main/about_us/4')?>">联系我们</a> <a
					<?=($about_type==5)?"class='on'":""?>
					href="<?=site_url('main/about_us/5')?>">了解更多</a>
			</div>
		<!--  test  -->
		
			<div class="gywm-2"  style="display:<?php if ($about_type!=0 ) { echo 'none';}?>">

			<table>
<tr>
<td><img style="margin: 2px"  src="<?=asset("images/logo.png")?>" /></td>
<td width="700">乐帮到家隶属广西乐帮网络科技有限公司，是一家互联网+家具售后服务平台，于2016年10月正式上线运营，目前专注于家具电商最后一公里的配送/安装/维修等服务，截止目前入驻平台实名认证的家具师傅数量已超过万名，服务区域已覆盖全国各市/区/县2000多个。<br />
				<br>
				随着电商的发展，传统家具商家纷纷开始转型开拓电商渠道，以削减中间成本。线上家具商家没有全国性自营的售后服务工人师傅团队，最后一公里的配送/安装/维修等服务需要求助于第三方。乐帮到家以共享经济的模式来连接商家与师傅，重新优化匹配家具售后服务的供需，通过师傅报价下单和客服定价批量下单两种服务方式来解决线上商家售后服务成本高、服务时效慢，维修没保障以及出单效率慢的痛点。<br />
				我们的使命：让家具售后服务更便捷。我们的愿景：成为家具售后服务第一平台；帮助家具师傅打造个人服务品牌第一平台。</td>
</tr>

<tr>
<td colspan="2" ><img style="margin: 2px"  src="<?=asset("images/shiming.png")?>" /></td>
</tr>
</table>
			</div>

			<!-- 公司文化 home_poto-->
			<div class="gywm-2"  style="display:<?php if ($about_type!=1 ) { echo 'none';}?>">
				<table style="border-collapse: separate; border-spacing: 10px;" >
					<tr>
						<td><span class="fwbz-2-1" style="margin: 2px">理念</span>
						   <hr>
							我们的使命：<br> 让家具售后服务更便捷。<br /> <br /> 我们的愿景：<br />
							成为家具售后服务第一平台；帮助家具师傅打造个人服务品牌第一平台。</td>
						<td><img style="margin: 2px" width="400px"
							src="<?=asset("images/wh.png")?>" /></td>
					</tr>
					<tr>
						<td><img width="400px" src="<?=asset("images/wh.png")?>" /></td>
						<td><span class="fwbz-2-1" style="padding: 4px">理念</span>  <hr>
							我们的使命：<br> 让家具售后服务更便捷。<br /> <br /> 我们的愿景：<br />
							成为家具售后服务第一平台；帮助家具师傅打造个人服务品牌第一平台。</td>
					</tr>
				</table>

			</div>


			<!-- 发展 home_poto-->
			<div class="gywm-2"  style="display:<?php if ($about_type!=2 ) { echo 'none';}?>">
				<!--    chenqinglin begin-->
		<div class="divcsswidth"> 
					<div class="contrainer header">
					<div class="w1000 tc body">
						<h1 class="header">乐帮到家大事件</h1>
				<div class="event_wrap clearfix">
				<div class="middle_line">
					<i class="first"></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i
						class="last"></i><i class="first"></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i
						class="last"></i>
				</div>
				
					<div class="list_left list">
					<span class="big_squre"><i>2015</i></span>
					<div class="ev_text ev_text_event ev_t2 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2015年7月</h3>
						<p>公司注册成立</p>
					</div>
					<div class="ev_text_odd ev_text ev_t4 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2015年8月</h3>
						<p>淘宝企业店接单渠道开通</p>
					</div>
					<div class="ev_text ev_text_event ev_t10 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2015年12月</h3>
						<p>第二届广西创业大赛第二名</p>
					</div>
				</div>
				<div class="list_right list">
					<span class="big_squre"><i>2016</i></span>
					<div class="ev_text ev_text_event ev_t2 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2016年3月</h3>
						<p>微信企业号派单系统上线</p>
					</div>
					<div class="ev_text_odd ev_text ev_t4 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>016年7月</h3>
						<p>服务区域覆盖全国超1500区/省</p>
					</div>
					<div class="ev_text ev_text_event ev_t6 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2016年9月</h3>
						<p>成功挂牌北部湾股权交易所</p>
					</div>
					<div class="ev_text_odd ev_text ev_t8 aActiveWid">
						<span class="small_squire"><i></i></span><span class="small_line"></span>
						<h3>2016年10月</h3>
						<p>平台2.0系统上线</p>
					</div>
				</div>
			
			</div>

		</div>

		</div>
		</div>
				<!-- end-->
			</div>

			<!-- 新闻 home_poto-->
			<div class="gywm-2"  style="display:<?php if ($about_type!=3 ) { echo 'none';}?>">

				<table style="border-collapse: separate; border-spacing: 10px;">
					<tr>
						<td><img width="302" height="227"
							src="<?=asset("images/new1.png")?>"></td>
						<td valign="top" width="593">
							<h3>我们的使命：让家具售后服务更便捷</h3> <br>
							<p>入驻平台的师傅可以服务到的区域，目前已全部覆盖全国长途物流能够到达的区域，实现服务覆盖全国各市/区/县</p>
						</td>
					</tr>
					<tr>
					<td colspan="2"><hr></td>
					</tr>
					<?php foreach ($new_list as $val) { ?>
					<tr>
						<td><img width="302" height="227" src="<?=asset("images/new1.png")?>"></td>
						<td valign="top"  width="593">
							<h3><?php echo $val["title"];?></h3> <br>
							<p>
							<?php echo $val["content"];?></p>
						</td>
					</tr>
					<tr>
					<td colspan="2"><hr></td>
					</tr>
					<?php  };?>
				</table>

			</div>

			<!-- 联系我们 home_poto-->
			<div class="gywm-2"  style="display:<?php if ($about_type!=4) { echo 'none';}?>">
				<img src="<?=asset("images/connectus.png")?>" />

			</div>

			<!-- 了解更多-->
			<div class="gywm-2"  style="display:<?php if ($about_type!=5 ) { echo 'none';}?>">
				<div class="htnr-b-6">
					<table width="1000" >
						<tr>
						<?php for ($x=0; $x<=2; $x++) { ?>
 									<td class="tab-b-1" width="279">
								<div class="thumbnail">
  									<img src="<?=asset("images/aboutmore.png")?>" />
  										<div class="caption">
    									<h3><a>精彩视频</a></h3>	
 											 </div>
									</div>
								</td>
					    <?php } ?> 
								
				
						</tr>
						<tr>
								<?php for ($x=0; $x<=2; $x++) { ?>
 									<td class="tab-b-1" width="279">
								<div class="thumbnail">
  									<img src="<?=asset("images/aboutmore.png")?>" />
  										<div class="caption">
    									<h3><a>精彩视频</a></h3>	
 											 </div>
									</div>
								</td>
					    <?php } ?> 
						</tr>
					</table>

					<p align="right">
						<a href="#">加载更多</a>
					</p>

				</div>
			</div>

		</div>
	</div>
</div>

<?php require 'application/views/basic/bottom.php'; ?>
