<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php require 'application/views/basic/header.php'; ?>
<?php require 'application/views/basic/top1.php'; ?>

    <div class="container">
        <div class="fxsf-1">
            <a href="#">乐帮到家</a>> <a href="#">发现师傅</a>
        </div>

        <div class="fxsf-2" id="div">
            <div class="fxsf-3 col-md-9">

                <form action="" method="get">
                    <div class="fxsf-3-1">服务区域：
                        <select name="province" id="province" >
                            <option></option>
                        </select>
                        <select name="city" id="city" >
                            <option ><?=$cityName?></option>
                        </select>
                        <select name="district" id="district" >
                            <option><?=$districtName?></option>
                        </select> 

                    <button type="submit" style="background: none;border:none;"><a>搜索</a></button>
                    <?php if (!empty($count)):?>
                    <span>该地区有<font color="#f00"><?=$count?></font>个师傅为你提供服务</span>
                    <?php endif;?>
                </div>
                </form>
                <div class="fxsf-3-2">服务类型：
                    <a <?=empty($service_type)?'class="on"':''?> href="?district=<?=$districtId?>&city=<?=$cityId?>&province=<?=$provinceId?>">不限</a>
                    <a <?=$service_type==1?'class="on"':''?> href="?district=<?=$districtId?>&city=<?=$cityId?>&province=<?=$provinceId?>&service_type=1">配送</a>
                    <a <?=$service_type=='1,2'?'class="on"':''?> href="?district=<?=$districtId?>&city=<?=$cityId?>&province=<?=$provinceId?>&service_type=1,2">配送+安装</a>
                    <a <?=$service_type==2?'class="on"':''?> href="?district=<?=$districtId?>&city=<?=$cityId?>&province=<?=$provinceId?>&service_type=2">安装</a>
                    <a <?=$service_type==3?'class="on"':''?> href="?district=<?=$districtId?>&city=<?=$cityId?>&province=<?=$provinceId?>&service_type=3">维修</a></div>

                <?php if (empty($list)):?>
                    <div style="text-align:center;font-size:20px;padding-top:50px;">请选择服务区域搜索发现师傅......</div>
                <?php else:?>
                    <?php foreach ($list as $row):?>
                <div class="fxsf-4">
                    <div class="col-md-2 fx-1"><a href="<?=site_url('order/master/introduce/'.$row['id'])?>"><img src="<?=$row['idcard_hold']?>" width="160px" height="120px" /></a></div>
                    <div class="col-md-3 fx-2"><span class="fx-2-1"><?=$row['real_name']?></span>
                        <span class="fx-2-1">保证金：<img src="<?=asset('images/fund_img.jpg')?>"   height = "20px"/><span class="fund-txt"><?=$row['assure_fund']?>元</span></span>
                        <span class="fx-2-1">信誉：<?=htmlspecialchars_decode($row['stars'])?></span>
                        <span class="fx-2-1" style="  display: none; color:#0fb7db">承诺<?=$row['extra_count']?>项服务</span></div>
                    <div class="col-md-2 fx-3">
                        <span class="fx-3-1">总接单： <span class="fx-3-11"><?=$row['order_count']?>单</span></span>
                        <span class="fx-3-1">总评分： <span class="fx-3-11"><?=$row['avg_score']?>分</span></span>
                        <span class="fx-3-1">好评率： <span class="fx-3-11"><?=$row['praise_rate']?>%</span></span>
                        <span class="fx-3-1">投诉记录： <span class="fx-3-12"><?=$row['complain_count']?>次</span></span>
                        <span class="fx-3-1"><a href="<?=site_url('order/master/introduce/'.$row['id'])?>"><u>累计评价(<?=$row['evaluate_count']?>)</u></a></span>
                    </div>
                    <div class="col-md-5 fx-4">
                        <span class="fx-4-1">所在位置：<?=$row['area_text']?></span>
                        <span class="fx-4-1">服务类型：家具类(<?=$row['service_type_text']?>)</span>
                        <span class="fx-4-1">服务区域：<?=$row['service_area_text']?></span>
                    </div>

                </div>
                        <?php endforeach ?>
                <?php endif;?>



                <div class="htfy" style="margin-top:25px;">
                    <?=@$__pagination_url?>
                </div>


            </div>
            <div class="fxsf-5 col-md-3">
                <div class="fx-r-1"><a href="<?=site_url('order/baojia')?>">发布订单获取师傅报价</a></div>
                <div class="fx-r-2">
                    <div class="fx-r-2-1">常见问题</div>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                    <li class="fx-r-2-2"><a href="#">办公转椅轮子怎么拆卸更换？</a></li>
                </div>
            </div>

        </div>



    </div>

<script type="text/javascript">
var $provinces = <?=$__provinces?>;
var $currentPName='<?=$provinceName?>';
var $provinceId='<?=$provinceId?>';
var $cityId='<?=$cityId?>';
var $cityName='<?=$cityName?>';
var $districtId='<?=$districtId?>';
var $districtName='<?=$districtName?>';

init_province();

//init province
function init_province(){
    var pro_html = "<option value='"+$provinceId+"'>"+$currentPName+"</option>";
    $.each($provinces[0], function(i, n){
        pro_html += "<option value='"+ i +"'>"+ n +"</option>";   
    });
    $("#province").html(pro_html);

    // search before setting city and district 
    if($provinceId != '') {
        initcity($provinceId,$cityId,$cityName,'1');
    }

    if($cityId != '') {
        initdistrict($cityId,$districtId,$districtName,'1');
    }
};

//city
$("#province").change(function(){
    var pro_id = $("#province").find("option:selected").val();
   initcity(pro_id,'','请选择市','');
});
function initcity(pro_id,city_id,city_name,default_c) {
     var html = "<option value='"+city_id+"'>"+city_name+"</option>";
     if(default_c != '') {
        html += "<option value='"+"'>"+'请选择市'+"</option>";
     }
    $.each($provinces[pro_id], function(i, n){
        html += "<option value='"+ i +"'>"+ n +"</option>";
    });
    $("#city").html(html);
}

//district
$("#city").change(function(){
    var city_id = $("#city").find("option:selected").val();
    initdistrict(city_id,'','请选择区','');
});
function initdistrict(city_id,district_id,district_name,default_d) {
    var html = "<option value='"+district_id+"'>"+district_name+"</option>";
    if(default_d != '') {
        html += "<option value='"+"'>"+'请选择区'+"</option>";
    }
    $.each($provinces[city_id], function(i, n){
        html += "<option value='"+ i +"'>"+ n +"</option>";
    });
    $("#district").html(html);
}


</script>
<?php require 'application/views/basic/bottom.php'; ?>

