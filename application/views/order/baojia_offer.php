<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php require 'application/views/basic/header.php'; ?>   
<?php require 'application/views/basic/top2.php'; ?>

<div class="nr-1">
	<div class="container">
		<a href="<?=site_url('main/index')?>">首页</a>》
		<a href="<?=site_url('order/index')?>">后台管理中心</a>》
		<a href="javascript:void(0);">报价订单详情</a>》
	</div>
</div>

<div class="ckfwjd">
    <div class="container ckfwjd-1">
        <div class="ckjd-1">
            <a href="<?=site_url('order/baojia_detail/'.$order_id)?>">报价订单详情</a>
            <a class="on" href="<?=site_url('order/baojia_offer/'.$order_id)?>">师傅报价<font color="#f00">(<?=$master_num?>)</font></a>
            <?php if($trace['merchant_status']>4){ ?>
            <a href="<?=site_url('order/baojia_trace/'.$order_id)?>">查看服务节点</a>
            <?php } ?>

            <?php if($trace['refund_status']==1){ ?>
            <span class="ckjd-1-1">
                <span class="ckjd-1-6">
                    <a href="<?=site_url('refund/detail/'.$order_id)?>">申请退款中 ></a>
                </span>
            </span>
            <?php }else if($trace['arbitrate_status']==1){ ?>
            <span class="ckjd-1-1">
                <span class="ckjd-1-6">
                    <a href="<?=site_url('refund/detail/'.$order_id)?>">申请仲裁中 ></a>
                </span>
            </span>
            <?php }else if($trace['except_status']>1){ ?>
            <span class="ckjd-1-1">
                <span class="ckjd-1-6">
                    <a href="<?=site_url('refund/detail/'.$order_id)?>" style="background-color:gray;">退款成功 ></a>
                </span>
            </span>
            <?php }else{ ?>

            <span class="ckjd-1-1">
                <?php if($trace['merchant_status']==6){ ?>
                <span id="confirm-count">
                    -天--时--分--秒将自动确认验收放款
                </span>

                <script type="text/javascript">
                    var timestamp = Date.parse(new Date()); 
                    var intDiff = <?=(config_item('auto_confirm_time')+$trace['finish_time'])?> - timestamp/1000;

                    $(function(){
                        timer(intDiff,0);
                    }); 
                </script>

                <span class="ckjd-1-2"> 
                    <a href="<?=site_url('order/baojia_detail/'.$order_id)?>">确认验收 ></a>
                </span>
                <?php } ?>

                <?php if($trace['merchant_status']>3){ ?>
                <?php if($trace['merchant_status']<7){ ?>
                <span class="ckjd-1-3">
                    <a href="<?=site_url('refund/add/'.$order_id)?>">申请退款</a>
                </span>
                <?php }else if($trace['evaluate_status']==0){ ?>
                <span class="ckjd-1-2"> 
                    <a href="<?=site_url('evaluate/add/'.$order_id)?>">评价师傅 ></a>
                </span>
                <?php }else if($trace['evaluate_status']==1){ ?>
                <span class="ckjd-1-3">
                    <a>你已评价师傅</a>
                </span>
                <?php } ?>
                <?php } ?>
            </span>
            <?php } ?>
        </div>

        <?php $hired_flag=0;foreach($master['base'] as $i => $val){ ?>
        <div class="ybj">
            <div class="ybj-1 col-md-4" >
                <div class="col-md-4 ybj-2" rel="<?=$val['id']?>" style="cursor:pointer;" >
                    <img src="<?=$val['idcard_hold']?>" />
                </div>
                <div class="col-md-8 ybj-3 "><!-- onclick="showphone()" -->
                    <div class="phone_st" rel="<?=$val['id']?>" ><?=$val['real_name']?> <?=$val['phone']?><img   width="25px" src="<?=asset("images/bj3.png")?>"  style="cursor:pointer;"/>
                    <br/></div>
                    保证金:
                    <?php if(empty($master['fund'][$i]['assure_fund'])){ ?>
                    <font color="#999">暂未缴纳</font></br>
                    <?php }else{ ?>
                    <img class="fund-img fund-top-1" src="<?php $fund_path = $master['fund'][$i]['assure_fund'] != 0 ?  '/images/fund_img.jpg' :'/images/fund_img_disable.jpg';  echo asset($fund_path); ?>"><span class="<?php if($master['fund'][$i]['assure_fund'] != 0){ echo 'fund-txt';} else { echo "fund-txt-disable";} ?>"><?=$master['fund'][$i]['assure_fund']?>元</span></br>
                    <?php } ?>
                    信誉:<?=$master['statistic'][$i]['__score_icon']?><br/>
                    <a class="promise_st" rel="<?=$val['id']?>"  style="cursor:pointer;"  >服务承诺</a>
                </div>
            </div>

            <div class="ybj-4 col-md-4">
                总单数：<span class="ybj-4-1"><?=$master['statistic'][$i]['order_count']?>单</span><br/>
                总评分：<span class="ybj-4-1"><?=$master['statistic'][$i]['score_sum']?>分</span><br/>
                好评率：<span class="ybj-4-1"><?=$master['statistic'][$i]['good_rat']?></span><br/>
                投诉记录：<span class="ybj-4-2"><?=$master['statistic'][$i]['complain_count']?>次</span><br/>
                <a href="<?=site_url('order/master/introduce/'.$val['id'])?>">累计评价(<?=$master['statistic'][$i]['evaluate_count']?>)</a>
            </div>

            <?php if($val['status']==0){ ?>
            <div class="ybj-5 col-md-4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;报价时间:<?=$val['off_time']?> <br><br>
                报价：<font color="#f00"><?=$val['price']?>元</font><br/><br/>
                <a style="cursor:pointer;<?php if ($trace['except_status'] == 1) { echo 'background-color:gray;'; }?>" class="hired-master" rel="<?=$val['real_name']?>（<?=$val['phone']?>）" data="<?=$val['id']?>" exst = "<?=$trace['except_status']?>">雇佣他</a>
            </div>
            <?php }else if($val['status']==1){ $hired_flag=$val['id']; ?>
            <div class="ybj-55 col-md-4">
                报价：<font color="#f00"><?=$val['price']?>元</font><br/>
                <a>已雇佣</a> <br/>
                <a style="width:108px;padding-left:10px;padding-right:10px;" href="<?=site_url('order/order_pay/'.$order_id)?>">托管费用</a>
            </div>
            <?php }else if($val['status']==2){ ?>
            <div class="ybj-55 col-md-4">
                报价：<font color="#f00"><?=$val['price']?>元</font><br/>
                <a>成功雇佣</a>
                <?php if($trace['merchant_status']>3 && $trace['merchant_status']<7){ ?>
                <br/><a href="<?=site_url('complain/add/'.$order_id)?>">投诉师傅</a>
                <?php } ?>
            </div>
            <?php }else if($val['status']==3){ ?>
            <div class="ybj-55 col-md-4">
                报价：<font color="#f00"><?=$val['price']?>元</font><br/>
                <a style="background-color:gray;">未被雇佣</a>
            </div>
            <?php } ?>

            <?php if (!empty($val['message'])):?>
            <div class="ybj-55 col-md-4">
                留言：<font color="#f00"><?=$val['message']?></font><br/>
            </div>
            <?php endif;?>
        </div>
        <?php } ?>

    </div>
</div>

<?php require 'application/views/basic/bottom.php'; ?>

<!--hired master-->
<div class="hire-master-pop" style="display:none;">
    <div class="lb_mask"></div>
    <div class="tt-2-hire">
        <div class="tt-2-1">雇佣师傅 <img class="close-pop" src="<?=asset("images/02418.png")?>" style="cursor:pointer;" /></div>
        <div class="tt-2-22">你确定雇佣<font color="#FFCC66">  </font>为你提供服务吗？</div>
        <div class="tt-2-4">提示：雇佣师傅后3天未付款将自动关闭！</div>
        <div class="tt-2-3"><a style="cursor:pointer;"  class="hire-confirm">雇佣付款</a><a class="on cancel-pop" style="cursor:pointer;" >取消</a></div>
    </div>
</div>

<!--rehired master-->
<div class="rehire-master-pop" style="display:none;">
    <div class="lb_mask"></div>
    <div class="tt-2-hire">
        <div class="tt-2-1">重新雇佣师傅 <img class="close-pop-re" src="<?=asset("images/02418.png")?>" style="cursor:pointer;" /></div>
        <div class="tt-2-22">你确定雇佣<font color="#FFCC66">  </font>为你提供服务吗？</div>
        <div class="tt-2-4">提示：雇佣师傅后3天未付款将自动关闭！</div>
        <div class="tt-2-3"><a style="cursor:pointer;"  class="hire-confirm">雇佣付款</a><a class="on cancel-pop-re" style="cursor:pointer;" >取消</a></div>
    </div>
</div>

<div class="phone-master-pop" style="display:none;">
    <div class="lb_mask"></div>         
      <div class="tt-2-hire">
        <div class="tt-2-1">紧急电话 <img class="close-pop-phone" src="<?=asset("images/02418.png")?>" style="cursor:pointer;" /></div>   

        <div class="tt-2-4" style="font-size: 5"><br><br> 如有需要可以拨打紧急号码：<font color="#000" size="5">  </font></div>    
    </div>
    </div>
</div>

<div class="promise-master-pop" style="display:none;">
   <div class="lb_mask"></div>         
      <div class="tt-2-promise">
        <div class="tt-2-1">承诺6项服务 <img class="close-pop-promise" src="<?=asset("images/02418.png")?>" style="cursor:pointer;" /></div>   
            <br><br>
    <div class="sfzs-wf">
      <span class="sfzs-wf-1" id = "extra_tmall_examine" > <img src="<?=asset("images/121.png")?>" />提供喵师傅核销</span>  
      <span class="sfzs-wf-1" id = "extra_storage"> <img src="<?=asset("images/121.png")?>" />提供货品仓储服务</span>        
      <span class="sfzs-wf-1" id="extra_move_free"> <img src="<?=asset("images/121.png")?>" />提供免费平移服务</span>
      <span class="sfzs-wf-1" id = "extra_finish_in_root"> <img src="<?=asset("images/121.png")?>" />
        货到后<font id = "extra_finish_in"></font>小时内完成任务
      </span>
      <span class="sfzs-wf-1" id = "extra_nothing_fee_root"> <img src="<?=asset("images/121.png")?>" />
        空跑费<font id = "extra_nothing_fee"></font>元/次
      </span>
      <span class="sfzs-wf-1" id = "extra_repair_free_root"> <img src="<?=asset("images/121.png")?>" />
        服务<font id = "extra_repair_free"></font>个月内免费维修
      </span>
      <span class="sfzs-wf-1" id = "extra_floor_free_root"> <img src="<?=asset("images/121.png")?>" />
        步梯在<font id = "extra_floor_free"></font>楼及以下免费搬楼
      </span>
      <span class="sfzs-wf-1" id = "extra_carry_fee_root"> <img src="<?=asset("images/121.png")?>" />
        搬楼费<font id = "extra_carry_fee"></font>元每件(25公斤以上)
      </span>   
     <span class="sfzs-wf-1" id ="extra_far_fee_root" > <img src="<?=asset("images/121.png")?>" />
        无物流地址提货，超过30公里以外的按<font id = "extra_far_fee"></font>元/公里加收
      </span>
    </div>   
    </div>
</div>


<script type="text/javascript">
    var $hiredFlag = <?=$hired_flag?>; 
    var $masterId = 0;
    var $hiredBox = $(".hire-master-pop");
    var $rehiredBox = $(".rehire-master-pop");

    var $phoneBox = $(".phone-master-pop");
    var $promiseBox = $(".promise-master-pop");

    $(".hired-master").click(function(){
        var msg = $(this).attr('rel');
        $masterId = $(this).attr('data');
        $exst = $(this).attr('exst');
        if($exst == 1) {
            return;
        }
        if($hiredFlag == 0){
            $hiredBox.find('font').html(msg);
            $hiredBox.show();
        }else{
            $rehiredBox.find('font').html(msg);
            $rehiredBox.show();
        }
    });

    //submit
    $(".hire-confirm").click(function(){
        $.ajax({
            type:"get",
            url:"<?=site_url('order/hire_master/'.$order_id)?>" + "/" + $masterId + "/" + $hiredFlag,
            success:function(msg){
                if(msg.status == 0){
                    window.location.href = "<?=site_url('order/order_pay/'.$order_id)?>";
                }else{
                    alert(msg.error);
                }
            }
        });
    })

    $(".close-pop, .cancel-pop").click(function(){
        $hiredBox.hide();
    });

    $(".close-pop-re, .cancel-pop-re").click(function(){
        $rehiredBox.hide();
    });

    $(".close-pop-phone").click(function(){
        $phoneBox.hide();
    });

    $(".close-pop-promise").click(function(){
        $promiseBox.hide();
    });


    $(".ybj-2").click(function(){
        var master_id = $(this).attr('rel');
        window.location.href = "<?=site_url('order/master/introduce')?>" + "/" + master_id;
    });

    $(".phone_st").click(function() {
        var msg_id = $(this).attr('rel');
                $.ajax({
                     type:"get",
                    url:"<?=site_url('order/get_back_phone')?>" + "/" +msg_id,
                    success:function(msg){
                        if(msg.status == 1){
                            var content = msg.data;
                            if(msg.data == null || (!msg.data)) {
                                content = '此师傅没有号码';
                            }
                           $phoneBox.find('font').html(content);
                            $phoneBox.show();
                        } else {
                            alert(msg.error);
                }
            }
        });

    });

    $(".promise_st").click(function() {
         var msg_id = $(this).attr('rel');
                $.ajax({
                     type:"get",
                    url:"<?=site_url('order/get_promise')?>" + "/" +msg_id,
                    success:function(msg){
                        if(msg.status == 1){
                            if(msg.data.extra_tmall_examine ==0) {
                                var extra_tmall_examine = $promiseBox.find("#extra_tmall_examine");
                                        extra_tmall_examine.hide();
                            }

                            if(msg.data.extra_storage ==0) {
                                var extra_storage = $promiseBox.find("#extra_storage");
                                        extra_storage.hide();
                            }

                             if(msg.data.extra_move_free ==0) {
                                var extra_move_free = $promiseBox.find("#extra_move_free");
                                        extra_move_free.hide();
                            }

                             $promiseBox.find("#extra_finish_in").html((!msg.data.extra_finish_in) ? 48: msg.data.extra_finish_in);

                            if(!msg.data.extra_repair_free) {
                                    $promiseBox.find("#extra_repair_free_root").hide();
                            } else {
                                    $promiseBox.find("#extra_repair_free").html(msg.data.extra_repair_free);
                            }

                            if(!msg.data.extra_nothing_fee) {
                                    $promiseBox.find("#extra_nothing_fee_root").hide();
                            } else {
                                 $promiseBox.find("#extra_nothing_fee").html(msg.data.extra_nothing_fee);
                            }

                            if(!msg.data.extra_floor_free) { 
                                    $promiseBox.find("#extra_floor_free_root").hide();
                            } else {
                                 $promiseBox.find("#extra_floor_free").html(msg.data.extra_floor_free);
                            }
                            
                            

                            if(!msg.data.extra_carry_fee) {
                                $promiseBox.find("#extra_carry_fee_root").hide();
                            } else {
                                $promiseBox.find("#extra_carry_fee").html(msg.data.extra_carry_fee)
                            }

                            if(!msg.data.extra_far_fee) {
                                 $promiseBox.find("#extra_far_fee_root").hide();
                            } else {
                                 $promiseBox.find("#extra_far_fee").html(msg.data.extra_far_fee)
                            }
                           
                            $promiseBox.show();
                        } else {
                            alert(msg.error);
                }
            }
        });
    });

    function timer(intDiff, type){
        window.setInterval(function(){
          var day = 0,
              hour = 0,
              minute = 0,
              second = 0;
          if(intDiff > 0){
            day = Math.floor(intDiff / (60 * 60 * 24));
            hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
            minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
            second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);

            $("#confirm-count").html(day + '天' + hour + '时' + minute + '分' + second + '秒');
            
            intDiff--;
          }
        }, 1000);
    }
</script>

